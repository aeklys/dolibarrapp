package com.example.dolibarrapp_projet;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText username, password, domain;

    private MainActivity mainActivity;

    private String tokenApi = null;

    private String domaine, IdNDF;

    private int Id;

    private long Long, Long2;

    public String getTokenApi() {
        return tokenApi; //Renvoie le token lorsque la fonction setTokenApi est appelée
    }

    public void setTokenApi(String token)
    {
        this.tokenApi = token;
    }

    public int getId() {
        return Id; //Renvoie l'identifiant du compte qui créer la note de frais lorsque la fonction getId est appelée
    }

    public void setId(int id) {
        this.Id = id;
    }
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) { //Création de l'activité principale
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Création de la vue de l'activité principale

        Button button = findViewById(R.id.button); //Initialise le bouton
        this.username = findViewById(R.id.editTextTextPersonName); //Initialise les champs à renseigner de l'application
        this.password = findViewById(R.id.editTextTextPassword);
        this.domain = findViewById(R.id.editTextDomaine);
        this.mainActivity = this;

        button.setOnClickListener(view -> {
                new Page_Accueil(mainActivity);

                String identifiant = username.getText().toString(); //Récupère le texte renseigné dans le champs de l'identifiant,
                String mdp = password.getText().toString(); //mot de passe,
                domaine = domain.getText().toString(); //domaine

                Token jetonconnexion = new Token(mainActivity, identifiant, mdp, domaine); //Appel de la fonction pour la récupération du token

                jetonconnexion.start(); //Démarre le run du thread de la fonction
            });
    }

    public long getLong() {
        return Long;
    }

    public void setLong(long aLong) {
        Long = aLong;
    }

    public long getLong2() {
        return Long2;
    }

    public void setLong2(long long2) {
        Long2 = long2;
    }

    public String getDomaine() {
        return domaine;
    }

    public String getIdNDF() {
        return IdNDF;
    }

    public void setIdNDF(String idNDF) {
        IdNDF = idNDF;
    }
}