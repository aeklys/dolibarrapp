package com.example.dolibarrapp_projet;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Page_Accueil {

    private MainActivity mainActivity;

    EditText utilisateur, dated, datef, budget;

    public Long date1, date2;

    public Page_Accueil(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.mainActivity.setContentView(R.layout.activity_page_accueil);

        utilisateur = mainActivity.findViewById(R.id.note_de_frais_utilisateur); //Initilise les champs à renseigner
        dated = mainActivity.findViewById(R.id.date_debut);
        datef = mainActivity.findViewById(R.id.date_fin);
        budget = mainActivity.findViewById(R.id.montant);

        Button button = mainActivity.findViewById(R.id.button2); //Lorsque le bouton est pressé la deuxième activité se lance
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String Utilisateur = utilisateur.getText().toString(); //Récupère le texte renseigné dans les champs de l'application
                    String DateD = dated.getText().toString();
                    String DateF = datef.getText().toString();
                    String Budget = budget.getText().toString();

                    Date dateD = new SimpleDateFormat("dd/MM/yyyy").parse(DateD); //Paterne de la date pour le timestamp (en secondes depuis 1970)
                    Date dateF = new SimpleDateFormat("dd/MM/yyyy").parse(DateF);

                    date1 = dateD.getTime()/1000; //Divise la date par 1000 (donc le timestamp) pour avoir la date voulue selon le parterne
                    date2 = dateF.getTime()/1000;

                    System.out.println("Date début : " + date1); //Affiche la date de début dans la console
                    System.out.println("Date de fin : " + date2); //Affiche la date de fin dans la console
                    System.out.println(Utilisateur); //Affiche le nom de l'utilisateur renseigné
                    System.out.println(Budget); //Affiche le montant de la note de frais

                    new Facture(mainActivity, Utilisateur, dateD, dateF, Budget, Page_Accueil.this); //Appelle la fonction pour créer une note de frais et la lance

                    mainActivity.setLong(date1 + 86400); //+86400 (3600 sec * 24 h) correspond à une journée en secondes
                    mainActivity.setLong2(date2 + 86400);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onEndFacture() {
        System.out.println("PUT");
        new Montant(mainActivity);
    }

}