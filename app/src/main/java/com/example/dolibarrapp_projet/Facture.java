package com.example.dolibarrapp_projet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class Facture extends Thread {

    private MainActivity mainActivity;

    private Date DateF, DateD;

    private String Utilisateur, Montant;

    private Page_Accueil page_accueil;

    private StringBuilder response;

    public Facture(MainActivity mainActivity, String utilisateur, Date dated, Date datef, String montant, Page_Accueil page_accueil) {
        this.mainActivity = mainActivity;
        this.page_accueil = page_accueil;
        this.Utilisateur = utilisateur;
        this.DateD = dated;
        this.DateF = datef;
        this.Montant = montant;
        this.start();
    }

    @Override
    public void run() {
        try {
            URL url = new URL ("http://"+mainActivity.getDomaine()+"/dolibarr/api/index.php/expensereports");

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("DOLAPIKEY", mainActivity.getTokenApi());
            urlConnection.setDoOutput(true);

            String jsonInputString = "{\"fk_user_author\": "+ mainActivity.getId() +", \"date_debut\": "+ mainActivity.getLong() +", \"date_fin\" : "+ mainActivity.getLong2() +"}";

            System.out.println(jsonInputString);

            try(OutputStream os = urlConnection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try(BufferedReader br = new BufferedReader(
                new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
                response = new StringBuilder();
                String responseLine = "", line = "";
                while ((line = br.readLine()) != null) {
                    responseLine = responseLine + line; //Contient l'id de la note de frais afin de gérer ses paramètres
                }
                mainActivity.setIdNDF(responseLine); //Renvoie l'id de la note de frais lorsque la fonction est appellée
                page_accueil.onEndFacture(); //Termine le run du thread afin de lancer le second run qui le suit
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
