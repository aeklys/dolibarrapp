package com.example.dolibarrapp_projet;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Token extends Thread {

    private MainActivity mainActivity;

    private final String identifiant, mdp, domaine;

    public Token(MainActivity mainActivity, String identifiant, String mdp, String domaine) {
        this.mainActivity = mainActivity;
        this.identifiant = identifiant; //ryan_txa
        this.mdp = mdp; // ryanteixeira
        this.domaine = domaine; // 10.0.51.244
    }

    @Override
    public void run() {
        try {
            URL url = new URL("http://"+domaine+"/dolibarr/api/index.php/login?login="+identifiant+"&password="+mdp); //URL pour se connecter à Dolibarr

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.connect();

            BufferedReader buffer = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String ligne = "", resultat = buffer.readLine();
            while (ligne != null) { //Récupère le texte affiché par l'api grâce à l'url donnée
                resultat += ligne + "\n";
                ligne = buffer.readLine();
            }
            System.out.println(resultat); //Affiche le texte dans la console

            JSONObject json = new JSONObject(resultat);

            json = json.getJSONObject("success"); //Tout ce qui est dans "success" est mit dans une variable

            mainActivity.setTokenApi(json.getString("token")); //Dans le "success" on retient le "token :"
            mainActivity.getTokenApi(); //Met le token seulement dans une fonction qui va renvoyer le token pour l'utiliser dans d'autres fichiers


            URL url2 = new URL("http://"+domaine+"/dolibarr/api/index.php/users/info"); //URL pour récupérer l'id du compte

            HttpURLConnection urlConnection2 = (HttpURLConnection) url2.openConnection();

            urlConnection2.setRequestMethod("GET");
            urlConnection2.setRequestProperty("Accept", "application/json");
            urlConnection2.setRequestProperty("DOLAPIKEY", mainActivity.getTokenApi());
            urlConnection2.connect();

            BufferedReader buffer2 = new BufferedReader(new InputStreamReader(urlConnection2.getInputStream()));
            InputStream in2 = new BufferedInputStream(urlConnection2.getInputStream());
            String ligne2 = "", resultat2 = buffer2.readLine();
            while (ligne2 != null) { //Récupère le texte affiché par l'api grâce à l'url donnée
                resultat2 += ligne2 + "\n";
                ligne2 = buffer2.readLine();
            }
            System.out.println(resultat2); //Affiche le texte dans la console

            JSONObject json2 = new JSONObject(resultat2);

            mainActivity.setId(json2.getInt("id")); //On retient l'id
            mainActivity.getId(); //Met l'id seulement dans une fonction qui va renvoyer le token pour l'utiliser dans d'autres fichiers
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
