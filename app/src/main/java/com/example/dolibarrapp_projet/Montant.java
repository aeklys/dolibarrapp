package com.example.dolibarrapp_projet;

import android.widget.EditText;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Montant extends Thread {

    private MainActivity mainActivity;

    EditText montant;

    public Montant(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.start(); //Lance le run lorsque la fonction Montant est appelée
    }

    @Override
    public void run() {

        montant = mainActivity.findViewById(R.id.montant); //Variable contenant le montant renseigné sur l'application

        try {
            URL url = new URL ("http://"+mainActivity.getDomaine()+"/dolibarr/api/index.php/expensereports/"+mainActivity.getIdNDF());

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("PUT");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("DOLAPIKEY", mainActivity.getTokenApi());
            urlConnection.setDoOutput(true);

            String jsonInputString = "{\"total_ttc\": "+montant.getText()+"}"; //Ajoute le montant voulu dans l'application à la note de frais directement dans Dolibarr

            System.out.println(jsonInputString); //Affiche dans la console le montant voulu pour voir s'il a bien été changé

            try(OutputStream os = urlConnection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try(BufferedReader br = new BufferedReader(
                new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
